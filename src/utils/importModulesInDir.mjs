import { promises as fs } from 'node:fs';
import path from 'node:path';
import { fileURLToPath, pathToFileURL } from 'node:url';

/**
 * Loads all modules in a directory.
 * @param {string} dirPath - The absolute path to the directory.
 * @param {Object} options - Options for loading modules.
 * @param {boolean} options.recursive - Whether to include subdirectories.
 * @returns {AsyncGenerator<Promise<object>>} A generator over the loaded modules.
 */
export async function* importModulesInDir(dirPath, { recursive } = {}) {
  const dirQueue = [pathToFileURL(dirPath)];
  while (dirQueue.length > 0) {
    const dir = dirQueue.pop();
    const entries = await fs.readdir(fileURLToPath(dir), { withFileTypes: true });

    for (const entry of entries) {
      if (entry.isFile() && /\.([mc]?js)$/.test(entry.name)) {
        const fileUrl = new URL(entry.name, dir + '/');
        yield import(fileUrl.href);
      } else if (recursive && entry.isDirectory()) {
        dirQueue.push(new URL(entry.name + '/', dir + '/'));
      }
    }
  }
}

/**
 * Loads modules from common repositories based on a configuration file.
 * @param {string} botRoot - The root directory of the bot.
 * @returns {Promise<{commands: Array, scripts: Array}>} A promise that resolves to an object containing arrays of command and script modules.
 */
export async function loadCommonModules(botRoot) {
  const configFilePath = path.resolve(botRoot, 'botCommons.json');
  const config = JSON.parse(await fs.readFile(configFilePath, 'utf-8'));
  const commonModules = { commands: [], scripts: [] };

  if (config.imports && Array.isArray(config.imports)) {
    for (const repo of config.imports) {
      const repoConfigPath = path.resolve(botRoot, `node_modules/${repo}/config.json`);
      const repoConfig = JSON.parse(await fs.readFile(repoConfigPath, 'utf-8'));

      // Load commands if they exist
      if (repoConfig.commands && Array.isArray(repoConfig.commands)) {
        for (const commandPath of repoConfig.commands) {
          const commandFilePath = path.resolve(botRoot, 'node_modules', repo, commandPath);
          const commandModule = await import(pathToFileURL(commandFilePath).href);
          commonModules.commands.push(commandModule);
        }
      }

      // Load scripts if they exist
      if (repoConfig.scripts && typeof repoConfig.scripts === 'object') {
        for (const [scriptPath, scriptNames] of Object.entries(repoConfig.scripts)) {
          const scriptFilePath = path.resolve(botRoot, 'node_modules', repo, scriptPath);
          const scriptModule = await import(pathToFileURL(scriptFilePath).href);
          for (const scriptName of scriptNames) {
            if (scriptModule[scriptName]) {
              commonModules.scripts.push({ name: scriptName, module: scriptModule[scriptName] });
            }
          }
        }
      }
    }
  }

  return commonModules;
}
